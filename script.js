class myArr {
  constructor(arg) {
    if (arguments.length > 1) {
      for (let i = 0; i < arguments.length; i++) {
        this[i] = arguments[i];
      }
      this.length = arguments.length;
    } else {
      this.length = arg || 0;
    }

    // Object.defineProperty(this, "_length", {
    //   enumerable: false
    // });
  }

  set length(val) {
    if (val < 0) return;
    if (val < this._length) {
      for (let i = val; i <= this._length - 1; i++) {
        delete this[i];
      }
    }
    this._length = val;
  }

  get length() {
    return this._length;
  }

  filter(callback) {
    const array = new myArr();
    for (let i = 0; i < this.length; ++i) {
      if (callback(this[i], i, this)) {
        array[i] = this[i];
      }
    }
    return array;
  }

  push(items) {
    for (
      let i = this.length, j = 0;
      i < this.length + arguments.length;
      ++i, ++j
    ) {
      this[i] = arguments[j];
    }
    this.length += arguments.length;
    return this.length;
  }

  pop() {
    let popElem = this[this.length - 1];
    delete this[this.length - 1];
    this.length = --this.length;
    return popElem;
  }

  shift() {
    let elem = this[0];
    for (var i = 0; i < this.length - 1; ++i) {
      this[i] = this[i + 1];
    }
    this.length = --this.length;
    delete this[this.length - 1];
    return elem;
  }

  unshift(item) {
    for (var i = this.length - 1; i > 0; --i) {
      this[i] = this[i - 1];
    }
    this[0] = item;
    item ? (this.length = ++this.length) : false;
    return this.length;
  }

  map(callback) {
    let array = new myArr();
    for (let i = 0; i < this.length; i++) {
      array[i] = callback(this[i], i, this);
      array.length++;
    }
    return array;
  }

  forEach(callback) {
    if (callback) {
      for (let i = 0; i < this.length; i++) {
        if (this[i]) {
          callback(this[i], i, this);
        }
      }
    }
  }

  sort(comparator) {
    const comp = comparator ? comparator(2, 1) : 0;
    const factor = 1.247;
    let gapFactor = this.length / factor;
    if (comp === 0) {
      while (gapFactor > 1) {
        const gap = Math.round(gapFactor);
        for (let i = 0, j = gap; j < this.length; i++, j++) {
          // if (this[i] == undefined || this[j] == undefined) continue;
          if (this[i] + "" > this[j] + "") {
            [arr[i], arr[j]] = [arr[j], arr[i]];
          }
        }
        gapFactor = gapFactor / factor;
      }
    }
    if (comp > 0) {
      while (gapFactor > 1) {
        const gap = Math.round(gapFactor);
        for (let i = 0, j = gap; j < this.length; i++, j++) {
          if (this[i] > this[j]) {
            [arr[i], arr[j]] = [arr[j], arr[i]];
          }
        }
        gapFactor = gapFactor / factor;
      }
    }
    if (comp < 0) {
      while (gapFactor > 1) {
        const gap = Math.round(gapFactor);
        for (let i = 0, j = gap; j < this.length; i++, j++) {
          if (this[i] < this[j]) {
            [arr[i], arr[j]] = [arr[j], arr[i]];
          }
        }
        gapFactor = gapFactor / factor;
      }
    }
    return this;
  }

  reduce(callback, initialValue) {
    let accumulator = initialValue === undefined ? undefined : initialValue;
    for (let i = 0; i < this.length; ++i) {
      if (accumulator !== undefined) {
        accumulator = callback(accumulator, this[i], i, this);
      } else {
        accumulator = this[i];
      }
    }
    return accumulator;
  }

  toString() {
    let string = new String();
    for (let i = 0; i < this.length; ++i) {
      if (this[i]) {
        string += this[i];
        string += i == this.length - 1 ? "" : ",";
      }
    }
    return string;
  }

  from(obj, mapFn, thisArg) {
    const array = new myArr();
    if (!obj.length) {
      throw new Error("Tak nelzya bratishka");
    } else {
      for (let i = 0; i < obj.length; ++i) {
        if (mapFn) {
          array[i] = mapFn(obj[i], i, thisArg);
        } else {
          array[i] = obj[i];
        }
        array.length = i + 1;
      }
    }
    return array;
  }

  [Symbol.iterator]() {
    const arr = this;
    let current = 0;
    return {
      next() {
        if (current <= arr.length - 1) {
          return {
            value: arr[current++],
            done: false
          };
        } else
          return {
            done: true
          };
      }
    };
  }
}

var arr = new myArr();

// for (let elem of arr) {
//   console.log(elem)
// }
console.log(arr);
